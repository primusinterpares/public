/*
 Copyright (c) 2008, Primus Inter Pares ltd
 http://www.primusinterpares.ltd.uk
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without modification, are permitted
 provided that the following conditions are met:
 
 * Redistributions of source code must retain the above copyright notice, this list of conditions
   and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions
   and the following disclaimer in the documentation and/or other materials provided with the
   distribution.
 * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse
   or promote products derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include "../parsexml.h"

static ParseXML_RC display(const ParseXML_Char* szNode, unsigned cContentLength, ParseXML_Stream ContentEnd)
{
	printf("Node: '%s'\nLength: %u\nContent: '", szNode, cContentLength);
	for (unsigned i=0; i<cContentLength; i++)
		putchar((ContentEnd->pNow-cContentLength)[i]);
	printf("'\n");
	return 0;
}

int main(int argc, char* argv[])
{
	for(argc--,argv++;argc--;argv++)
	{
		FILE* pXML = fopen(*argv, "rt");
		if (!pXML)
		{
			fprintf(stderr, "Failed to open file '%s'\n", *argv);
			return -1;
		}

		fseek(pXML, 0, SEEK_END);
		unsigned cFile = ftell(pXML);
		fseek(pXML, 0, SEEK_SET);

		char* aFile = new char[cFile];
		cFile = fread(aFile, sizeof(char), cFile, pXML);

		ParseXML_Stream sStream = new struct ParseXML_Stream_Struct;
		sStream->pNow = aFile;
		sStream->pEnd = aFile+cFile;

		ParseXML_RC rc = ParseXML(sStream, display);
		if (ParseXML_Failed(rc))
		{
			fprintf(stderr, "Parse failed rc=%u\n", rc);
			return -3;
		}
	}

	return 0;
}

