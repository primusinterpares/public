/*
 Copyright (c) 2008, Primus Inter Pares ltd
 http://www.primusinterpares.ltd.uk
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without modification, are permitted
 provided that the following conditions are met:
 
 * Redistributions of source code must retain the above copyright notice, this list of conditions
   and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions
   and the following disclaimer in the documentation and/or other materials provided with the
   distribution.
 * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse
   or promote products derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* XML micro-parser */

#include "parsexml.h"

/*extern*/ ParseXML_Char ParseXML_Path[ParseXML_XPath_Max];
#define ParseXML_CheckPath(n,c) do { if ((n+c) > ParseXML_Path+ParseXML_XPath_Max) return ParseXML_Failure_TooDeep; } while(0)

#define ParseXML_Peek_Whitespace(_s) (ParseXML_Peek(_s)==0x20||ParseXML_Peek(_s)==0x0D||ParseXML_Peek(_s)==0x0A||ParseXML_Peek(_s)==0x09)

static ParseXML_RC ParseXML_Text(ParseXML_Stream Content, ParseXML_Char terminate); 
static ParseXML_RC ParseXML_Elements(ParseXML_Stream Content, ParseXML_Output pfn_Callback, ParseXML_Char* node);
static ParseXML_RC ParseXML_Node(ParseXML_Stream Content, ParseXML_Output pfn_Callback, ParseXML_Char* node);
static ParseXML_RC ParseXML_Attributes(ParseXML_Stream Content, ParseXML_Output pfn_Callback, ParseXML_Char* node);

/* Parse request */
extern ParseXML_RC ParseXML(ParseXML_Stream Content, ParseXML_Output pfn_Callback)
{
	ParseXML_RC rc;

	/* Setup initial path */
	*ParseXML_Path = (ParseXML_Char)'/';
	
	/* Kick off recursive descent parser */
	rc = ParseXML_Elements(Content, pfn_Callback, ParseXML_Path+1);
	if (ParseXML_Failed(rc))
		return rc;

	/* Check at end of content */
	if (!ParseXML_End(Content))
		return ParseXML_Failure_BadXML;

	return 0;
}

static ParseXML_RC ParseXML_Text(ParseXML_Stream Content, ParseXML_Char terminate)
{
	unsigned cText = 0;
	for(;;)
	{
		/* End of stream? */
		if (ParseXML_End(Content))
			return cText;

		/* XXX - CDATA etc. */

		/* End of text? */
		if (ParseXML_Peek(Content) == terminate)
			return cText;

		ParseXML_Accept(Content);
		cText++;
	}
}
		
/* Parse elements list */
static ParseXML_RC ParseXML_Elements(ParseXML_Stream Content, ParseXML_Output pfn_Callback, ParseXML_Char* node)
{
	for (;;)
	{
		/* Empty? */
		if (ParseXML_End(Content))
			return 0;
	
		/* Look for a '<' */
		if (ParseXML_Peek(Content) == '<')
		{
			ParseXML_Accept(Content);
			
			/* Check for '/' to terminate elements */
			if (ParseXML_End(Content))
				return ParseXML_Failure_BadXML;
			if (ParseXML_Peek(Content) == '/')
				return 0;
			else
			{
				/* Parse a node */
				ParseXML_RC rc = ParseXML_Node(Content, pfn_Callback, node);
				if (ParseXML_Failed(rc))
					return rc;
			}
		}
		else
		{
			/* Setup node */
			static const char text[] = "text()\0";
			unsigned i=0;
			ParseXML_RC rc;

			ParseXML_CheckPath(node, sizeof(text));
			do
				node[i] = (ParseXML_Char)text[i];
			while (text[i++]);

			/* Parse text */
			rc = ParseXML_Text(Content, (ParseXML_Char)'<');

			/* Callback */
			rc = pfn_Callback(node, rc, Content);
		}
	}
}

/* Parse node after '<' but not '</' */
static ParseXML_RC ParseXML_Node(ParseXML_Stream Content, ParseXML_Output pfn_Callback, ParseXML_Char* node)
{
	/* Already accepted '<' and checked not '</' */
	unsigned iEntryContent = ParseXML_Index(Content)-1;
	ParseXML_ASSERT(!ParseXML_End(Content));
	ParseXML_ASSERT(ParseXML_Peek(Content) != (ParseXML_Char)'/');

	/* Comment or Processing-Instruction element? */
	if (ParseXML_Peek(Content) == (ParseXML_Char)'?'
	 || ParseXML_Peek(Content) == (ParseXML_Char)'!')
	{
		/* Setup node */
		if (ParseXML_Peek(Content) == (ParseXML_Char)'!')
		{
			static const char comment[] = "comment()\0";
			unsigned i=0;
			ParseXML_CheckPath(node, sizeof(comment));
			do
				node[i] = (ParseXML_Char)comment[i];
			while (comment[i++]);
		}
		else
		{
			static const char processor[] = "processing-instruction()\0";
			unsigned i=0;
			ParseXML_CheckPath(node, sizeof(processor));
			do
				node[i] = (ParseXML_Char)processor[i];
			while (processor[i++]);
		}

		/* Find end of comment/instruction */
		while (ParseXML_Accept(Content) != (ParseXML_Char)'>')
			if (ParseXML_End(Content))
				return ParseXML_Failure_BadXML;
	}
	else /* Tag */
	{
		ParseXML_RC rc;

		/* Setup node */
		unsigned i=0;
		for(;;)
		{
			ParseXML_CheckPath(node, i+2);
			node[i++] = ParseXML_Accept(Content);
			if (ParseXML_End(Content))
				return ParseXML_Failure_BadXML;
			if (ParseXML_Peek(Content) == (ParseXML_Char)'>'
			 || ParseXML_Peek(Content) == (ParseXML_Char)'/'
			 || ParseXML_Peek_Whitespace(Content))
				break;
		}
		node[i++] = (ParseXML_Char)'/';

		/* Tell the output that we're descending via a zero-length placeholder */
		node[i] = 0;
		rc = pfn_Callback(node, 0, Content);
		if (ParseXML_Failed(rc))
			return rc;

		/* Process attributes */
		rc = ParseXML_Attributes(Content, pfn_Callback, node+i);
		if (ParseXML_Failed(rc))
			return rc;

		/* Close tag? */
		if (ParseXML_Peek(Content) == (ParseXML_Char)'/')
		{
			ParseXML_Accept(Content);
			node[--i] = 0;
		}
		else /* Complex tag... */
		{
			if (ParseXML_Accept(Content) != (ParseXML_Char)'>')
				return ParseXML_Failure_BadXML;

			/* Process sub-nodes */
			rc = ParseXML_Elements(Content, pfn_Callback, node+i);
			if (ParseXML_Failed(rc))
				return rc;

			/* Match tag close */
			node[--i] = 0;
			if (ParseXML_Accept(Content) != (ParseXML_Char)'/')
				return ParseXML_Failure_BadXML;
			for(i=0; node[i] != 0; i++)
			{
				if (ParseXML_End(Content)
				 || ParseXML_Accept(Content) != node[i])
					return ParseXML_Failure_BadXML;
			}

			/* Remove trailing whitespace */
			for(;;)
			{
				if (ParseXML_End(Content))
					return ParseXML_Failure_BadXML;
				if (!ParseXML_Peek_Whitespace(Content))
					break;
				ParseXML_Accept(Content);
			}
		}

		if (ParseXML_End(Content)
		 || ParseXML_Accept(Content) != (ParseXML_Char)'>')
			return ParseXML_Failure_BadXML;

		/* All ok - return node content */
	}	

	/* Callback */
	return pfn_Callback(node, ParseXML_Index(Content)-iEntryContent, Content);
}

/* Parse attribute list after <TAG */
static ParseXML_RC ParseXML_Attributes(ParseXML_Stream Content, ParseXML_Output pfn_Callback, ParseXML_Char* node)
{
	/* Setup node */
	ParseXML_CheckPath(node,1);
	node[0] = '@';

	for(;;)
	{
		unsigned i=1;

		/* Strip whitespace */
		for(;;)
		{
			if (ParseXML_End(Content))
				return ParseXML_Failure_BadXML;
			if (!ParseXML_Peek_Whitespace(Content))
				break;
			ParseXML_Accept(Content);
		}
	
		/* End of attributes? */
		if (ParseXML_Peek(Content) == (ParseXML_Char)'/'
		 || ParseXML_Peek(Content) == (ParseXML_Char)'>')
		 	return 0;

		/* Parse attribute */
		do
		{
			if (ParseXML_End(Content))
				return ParseXML_Failure_BadXML;
			ParseXML_CheckPath(node,1);
			node[i++] = ParseXML_Accept(Content);
		} while (node[i-1] != (ParseXML_Char)'=');
		node[i-1] = 0;
		
		/* Parse value */
		if (ParseXML_End(Content)
		 || ParseXML_Accept(Content) != (ParseXML_Char)'"')
			return ParseXML_Failure_BadXML;
		else
		{
			ParseXML_RC rc = ParseXML_Text(Content, (ParseXML_Char)'"');
			if (ParseXML_Failed(rc))
				return rc;
			if (ParseXML_End(Content))
				return ParseXML_Failure_BadXML;

			/* Callback */
			rc = pfn_Callback(node, rc, Content);
			if (ParseXML_Failed(rc))
				return rc;
			
			/* Drop closing quote */
			ParseXML_Accept(Content);
		}
	}		
}

