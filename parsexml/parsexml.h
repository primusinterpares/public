/*
 Copyright (c) 2008, Primus Inter Pares ltd
 http://www.primusinterpares.ltd.uk
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without modification, are permitted
 provided that the following conditions are met:
 
 * Redistributions of source code must retain the above copyright notice, this list of conditions
   and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions
   and the following disclaimer in the documentation and/or other materials provided with the
   distribution.
 * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse
   or promote products derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* XML micro-parser */

#ifdef __cplusplus
extern "C" {
#endif

#include <assert.h>
#define ParseXML_ASSERT(_exp) assert(_exp)

typedef char ParseXML_Char;				/* Character type */
#define ParseXML_XPath_Max 128			/* Maximum parse depth */

typedef int ParseXML_RC;
#define ParseXML_Failed(rc) ((rc)<0)	/* -ve is failure */
#define ParseXML_Failure_BadXML -1		/* Input isn't valid XML */
#define ParseXML_Failure_TooDeep -2		/* Input structure too deep for ParseXML_XPath_Max */

/* Memory based stream */
typedef struct ParseXML_Stream_Struct
{
	const ParseXML_Char* pNow;
	const ParseXML_Char* pEnd;
} *ParseXML_Stream;

#define ParseXML_Index(_s) ((unsigned)((_s)->pNow))	/* Used for relative positions only */
#define ParseXML_End(_s) ((_s)->pEnd==(_s)->pNow)	/* End-of-stream test */
#define ParseXML_Peek(_s) (*(_s)->pNow)				/* 1 character look-ahead */
#define ParseXML_Accept(_s) (*(_s)->pNow++)			/* Move forward in stream */

/* Node callback */
extern ParseXML_Char ParseXML_Path[];	/* Current XPath */
typedef ParseXML_RC (*ParseXML_Output)(const ParseXML_Char* szNode, unsigned cContentLength, ParseXML_Stream ContentEnd);

/* Parse request */
extern ParseXML_RC ParseXML(ParseXML_Stream Content, ParseXML_Output pfn_Callback);

#ifdef __cplusplus
};
#endif
