/* 
 * File:   bbp_pi.c
 * Author: Chris St John
 *
 * Bailey-Borwein-Plouffe formula for pi implemented in ANSI 'C'
 * by Primus Inter Pares ltd. Public domain without warranty.
 *
 * This is the code snippet that appears on the back of our business cards
 */

#include <stdio.h>
main()
{ long double a=0,d=1,n=0,h;
  while(n<128)
  { h=d*(4/(n+1)-2/(n+4)-1/(n+5)-1/(n+6));
    a+=h; d/=16.0; n+=8.0;
    while(h<0.001)
    { putchar('0'+(char)a);
      a-=(char)a;
      if (n<30) putchar('.');
      a*=10.0; d*=10.0; h*=10.0;
} } }