/*
 Copyright (c) 2009, Primus Inter Pares ltd
 http://www.primusinterpares.ltd.uk
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without modification, are permitted
 provided that the following conditions are met:
 
 * Redistributions of source code must retain the above copyright notice, this list of conditions
   and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions
   and the following disclaimer in the documentation and/or other materials provided with the
   distribution.
 * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse
   or promote products derived from this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// A simple 5x5 alphanumeric font - lower case letters are mapped to capitals

#define SimpleFont_Height 5
#define SimpleFont_Width 5
#define SimpleFont_Entries 64

#define SimpleFont_Pixel(_c,_x,_y) (0x0F & (SimpleFont_Raster[(_y)+SimpleFont_Map[(_c)]*SimpleFont_Height]>>(4*(SimpleFont_Width-(_x)-1)))

const int SimpleFont_Map[256] =
{
	63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 
	63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63,		// CTRL chars - Filler

	0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,								// Space + Basic symbols
	16,17,18,19,20,21,22,23,24,25,										// Numbers 0..9
	26,27,28,29,30,31,32												// More symbols ..@

	33,34,35,36,37,38,39,40,41,42,43,44,45,
	46,47,48,49,50,51,52,53,54,55,56,57,58,								// Upper-case Alphabet

	59,60,61,62,63,														// More symbols

	63,	// Filler

	33,34,35,36,37,38,39,40,41,42,43,44,45,
	46,47,48,49,50,51,52,53,54,55,56,57,58,								// Upper-case Alphabet

	// Fillers
	63, 63, 63, 63, 63,
	63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63,
	63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63,
	63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63,
	63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63,
	63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63,
	63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63,
	63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63,
	63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63, 63,
};

const int SimpleFont_Raster[SimpleFont_Entries * SimpleFont_Height] =
{
	0x00000,
	0x00000,
	0x00000,
	0x00000,
	0x00000,

	0x00100,
	0x01110,
	0x00100,
	0x00000,
	0x00100,

	0x01010,
	0x01010,
	0x00000,
	0x00000,
	0x00000,

	0x01010,
	0x11111,
	0x01010,
	0x11111,
	0x01010,

	0x01111,
	0x10100,
	0x01110,
	0x00101,
	0x11110,

	0x10011,
	0x01001,
	0x00100,
	0x10010,
	0x11001,

	0x01100,
	0x10010,
	0x01101,
	0x10010,
	0x01111,

	0x00100,
	0x00100,
	0x00000,
	0x00000,
	0x00000,

	0x00100,
	0x01000,
	0x01000,
	0x01000,
	0x00100,

	0x00100,
	0x00010,
	0x00010,
	0x00010,
	0x00100,

	0x10101,
	0x01110,
	0x11011,
	0x01110,
	0x10101,

	0x00100,
	0x00100,
	0x11111,
	0x00100,
	0x00100,

	0x00000,
	0x00000,
	0x00000,
	0x00110,
	0x01100,

	0x00000,
	0x00000,
	0x11111,
	0x00000,
	0x00000,

	0x00000,
	0x00000,
	0x00000,
	0x00110,
	0x00110,

	0x00001,
	0x00010,
	0x00100,
	0x01000,
	0x10000,

	0x01110,
	0x10011,
	0x10101,
	0x11001,
	0x01110,

	0x00100,
	0x01100,
	0x00100,
	0x00100,
	0x01110,

	0x01110,
	0x10001,
	0x00110,
	0x11000,
	0x11111,

	0x01110,
	0x10001,
	0x00110,
	0x10001,
	0x01110,

	0x00110,
	0x01010,
	0x10010,
	0x11111,
	0x00010,

	0x11111,
	0x10000,
	0x11110,
	0x00001,
	0x11110,

	0x01110,
	0x10000,
	0x11110,
	0x10001,
	0x01110,

	0x11111,
	0x00010,
	0x00100,
	0x00100,
	0x00100,

	0x01110,
	0x10001,
	0x01110,
	0x10001,
	0x01110,

	0x01110,
	0x10001,
	0x01111,
	0x00001,
	0x01110,

	0x00000,
	0x00100,
	0x00000,
	0x00100,
	0x00000,

	0x00000,
	0x00100,
	0x00000,
	0x00100,
	0x01000,

	0x00011,
	0x01100,
	0x10000,
	0x01100,
	0x00011,

	0x00000,
	0x11111,
	0x00000,
	0x11111,
	0x00000,

	0x11000,
	0x00110,
	0x00001,
	0x00110,
	0x11000,

	0x01110,
	0x10001,
	0x00110,
	0x00000,
	0x00100,

	0x01110,
	0x10001,
	0x01101,
	0x10101,
	0x11110,

	0x00100,
	0x01010,
	0x11111,
	0x10001,
	0x10001,

	0x11110,
	0x10001,
	0x11110,
	0x10001,
	0x11110,

	0x01110,
	0x10001,
	0x10000,
	0x10001,
	0x01110,

	0x11100,
	0x10010,
	0x10001,
	0x10010,
	0x11100,

	0x11111,
	0x10000,
	0x11100,
	0x10000,
	0x11111,

	0x11111,
	0x10000,
	0x11100,
	0x10000,
	0x10000,

	0x01110,
	0x10000,
	0x10111,
	0x10001,
	0x01110,

	0x10001,
	0x10001,
	0x11111,
	0x10001,
	0x10001,

	0x11111,
	0x00100,
	0x00100,
	0x00100,
	0x11111,

	0x11111,
	0x00010,
	0x00010,
	0x10010,
	0x01110,

	0x10001,
	0x10010,
	0x11100,
	0x10010,
	0x10001,

	0x10000,
	0x10000,
	0x10000,
	0x10000,
	0x11111,

	0x10001,
	0x11011,
	0x10101,
	0x10101,
	0x10001,

	0x10001,
	0x11001,
	0x10101,
	0x10011,
	0x10001,

	0x01110,
	0x10001,
	0x10001,
	0x10001,
	0x01110,

	0x11110,
	0x10001,
	0x11110,
	0x10000,
	0x10000,

	0x01110,
	0x10001,
	0x10101,
	0x10011,
	0x01110,

	0x11110,
	0x10001,
	0x11110,
	0x10001,
	0x10001,

	0x01111,
	0x10000,
	0x01110,
	0x00001,
	0x11110,

	0x11111,
	0x00100,
	0x00100,
	0x00100,
	0x00100,

	0x10001,
	0x10001,
	0x10001,
	0x10001,
	0x01110,

	0x10001,
	0x10001,
	0x01010,
	0x01010,
	0x00100,

	0x10001,
	0x10101,
	0x10101,
	0x11011,
	0x01010,

	0x10001,
	0x01010,
	0x00100,
	0x01010,
	0x10001,

	0x10001,
	0x01010,
	0x00100,
	0x00100,
	0x00100,

	0x11111,
	0x00010,
	0x00100,
	0x01000,
	0x11111,

	0x01110,
	0x01000,
	0x01000,
	0x01000,
	0x01110,

	0x00100,
	0x01010,
	0x10001,
	0x00000,
	0x00000,

	0x01110,
	0x00010,
	0x00010,
	0x00010,
	0x01110,

	0x00000,
	0x00000,
	0x00000,
	0x00000,
	0x11111,

	0x11111,
	0x10101,
	0x11111,
	0x10101,
	0x11111
};
