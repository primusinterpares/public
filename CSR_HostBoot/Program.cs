/*
 * Copyright (c) 2014 Primus Inter Pares ltd
 * All rights reserved.
 * 
 * http://primusinterpares.co.uk
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may
 * be used to endorse or promote products derived from this software without specific
 * prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using PRIMUS;

namespace CSR_SPI
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 1)
                try
                {
                    Download(args[0]);
                }
                catch (System.Exception x)
                {
                    System.Console.WriteLine("ERROR: " + x.Message);
                }
            else
                System.Console.WriteLine("usage: CSR_HostBoot <filename.xuv>");
        }

        /// <summary>
        /// Lifted from csr1k_host_boot.c
        /// </summary>
        static void Download(string xuvFilename)
        {
            // Load XUV
            var image = CsrDebugSPI.ReadImage(new System.IO.StreamReader(xuvFilename));

            // Initialise SPI interface - 50kHz clock (seems to work upto 1MHz)
            var spi = new CsrDebugSPI(50000);
            while (!spi.IsOnline)
                System.Console.WriteLine("OFFLINE");

            // Reset chip
            spi.Write(0xF82F, 1);
            System.Threading.Thread.Sleep(1000);

            // Stop the processor
            spi.Write(0xF81D, 2);

            // Write code blocks
            UInt16 blk_address = 0;
            var blk_data = new List<UInt16>();
            foreach (var d in image)
            {
                // Flush block?
                if (blk_data.Count >= 16     // Max words to write in one block
                 || d.Key != blk_address + blk_data.Count)
                {
                    // Write current block
                    if (blk_data.Count != 0)
                    {
                        while (!spi.WriteVerify(blk_address, blk_data))
                            System.Console.WriteLine("VERIFY");

                        System.Console.Write("Block: 0x" + blk_address.ToString("X4") + "\r");
                    }

                    // Start a new block
                    blk_data.Clear();
                    blk_address = d.Key;
                }

                // Append to block
                blk_data.Add(d.Value);
            }

            // Write final block
            if (blk_data.Count != 0)
                while(!spi.WriteVerify(blk_address, blk_data))
                    System.Console.WriteLine("VERIFY");

            System.Console.WriteLine("STARTING");

            // Magic toggle
            spi.Write(0x0018, 1);
            spi.Write(0x0018, 0);

            // Set PC to zero
            spi.Write(0xFFEA, 0);
            spi.Write(0xFFE9, 0);

            // And GO (which is really an "un-stop" :)
            spi.Write(0xF81D, 0);
        }
    }
}
