﻿/*
 * Copyright (c) 2011 Primus Inter Pares ltd
 * All rights reserved.
 * 
 * http://primusinterpares.co.uk
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may
 * be used to endorse or promote products derived from this software without specific
 * prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.Text;

namespace PRIMUS
{
    class SPI : IDisposable
    {
        private FTD2XX_NET.FTDI ftdi;
        private List<byte> transmit;
        private List<byte> receive;

        /// <summary>
        /// Check the result of a FTD2XX_NET operation
        /// </summary>
        /// <param name="ftStatus"></param>
        private void CHECK(FTD2XX_NET.FTDI.FT_STATUS ftStatus)
        {
            if (ftStatus != FTD2XX_NET.FTDI.FT_STATUS.FT_OK)
                throw new System.Exception("FTDI error: "+ftStatus.ToString());
        }

        /// <summary>
        /// Initialise and open a port
        /// </summary>
        /// <param name="clock">SPI clock rate required</param>
        /// <param name="index">device index</param>
        public SPI(uint clock = 1000000, uint index = 0)
        {
            try
            {
                this.ftdi = new FTD2XX_NET.FTDI();
                CHECK(this.ftdi.OpenByIndex(index));
            }
            catch
            {
                this.ftdi = null;
                throw;
            }

            // Need 2x clock rate for bit bashing
            CHECK(this.ftdi.SetBaudRate(clock / 4));

            // Set outputs high
            this.bits = SPI_OUT_PINS;
            CHECK(this.ftdi.SetBitMode(SPI_OUT_PINS, FTD2XX_NET.FTDI.FT_BIT_MODES.FT_BIT_MODE_SYNC_BITBANG));
            this.transmit = new List<byte>();
            this.OUTPUT = this.bits;
            this.Go();
        }

        /// <summary>
        /// Implements IDisposable
        /// </summary>
        public void Dispose()
        {
            if (this.ftdi != null)
            {
                this.ftdi.Close();
                this.ftdi = null;
            }
        }

        private byte bits;
        private const byte SPI_MOSI = 0x01; // D0, TX
        private const byte SPI_MISO = 0x02; // D1, RX
        private const byte SPI_CLK = 0x08;  // D3, CTS
        private const byte SPI_nCS = 0x10;  // D4, DTR
        private const byte SPI_OUT_PINS = SPI_CLK | SPI_MOSI | SPI_nCS;
        private const byte SPI_IN_PINS = SPI_MISO;

        /// <summary>
        /// Bit-Bash output
        /// </summary>
        private byte OUTPUT
        {
            get
            {
                return this.bits;
            }
            set
            {
                this.bits = value;
                this.transmit.Add(value);
            }
        }

        /// <summary>
        /// Bit-Bash input
        /// </summary>
        private byte INPUT
        {
            get
            {
                byte rx = this.receive[0];
                this.receive.RemoveAt(0);
                return rx;
            }
        }

        /// <summary>
        /// Set chip select
        /// </summary>
        public bool CS
        {
            set
            {
                byte output = this.OUTPUT;
                if (!value)  // Invert
                    output |= SPI_nCS & SPI_OUT_PINS;
                else
                    output &= SPI_nCS ^ SPI_OUT_PINS;
                if (output != this.OUTPUT)
                    this.OUTPUT = output;
            }
        }

        /// <summary>
        /// Clock data bit out
        /// </summary>
        public bool MOSI
        {
            set
            {
                byte output = this.OUTPUT;
                if (value)
                    output |= SPI_MOSI & SPI_OUT_PINS;
                else
                    output &= SPI_MOSI ^ SPI_OUT_PINS;
                output ^= SPI_CLK;
                this.OUTPUT = output;
                output ^= SPI_CLK;
                this.OUTPUT = output;
            }
        }

        /// <summary>
        /// Recover data bit in
        /// </summary>
        public bool MISO
        {
            get
            {
                // Look for the next CLK falling transition
                bool high = false;
                for(;;)
                {
                    byte input = this.INPUT;

                    // Chip must be selected...
                    if ((input & SPI_nCS) != 0)
                        high = false;
                    else if (!high)
                        high = (input & SPI_CLK) != 0;
                    else if ((input & SPI_CLK) == 0)
                        return (input & SPI_MISO) != 0;
                }
            }
        }

        /// <summary>
        /// Transmit a byte
        /// </summary>
        /// <param name="b"></param>
        public void TX(byte b)
        {
            for (uint i = 0; i < 8; i++)
            {
                this.MOSI = (b & 0x80) != 0;
                b <<= 1;
            }
        }

        /// <summary>
        /// Recover a byte
        /// </summary>
        /// <returns></returns>
        public byte RX()
        {
            byte b = 0;
            for (uint i = 0; i < 8; i++)
            {
                b <<= 1;
                if (this.MISO)
                    b |= 1;
            }
            return b;
        }

        /// <summary>
        /// Send clock pulse specifying edge
        /// </summary>
        /// <param name="edge">false = falling edge</param>
        public void Clock(bool edge)
        {
            if (!edge)
                this.OUTPUT |= SPI_CLK & SPI_OUT_PINS;
            this.OUTPUT &= SPI_CLK ^ SPI_OUT_PINS;
            if (edge)
                this.OUTPUT |= SPI_CLK & SPI_OUT_PINS;
        }

        /// <summary>
        /// Send clock pulse, in whatever polarity
        /// </summary>
        public void Clock()
        {
            this.OUTPUT ^= SPI_CLK;
            this.OUTPUT ^= SPI_CLK;
        }

        /// <summary>
        /// Perform queued transaction
        /// </summary>
        public void Go()
        {
            // Capture transaction
            byte[] tx = this.transmit.ToArray();
            this.transmit.Clear();

            // Write
            uint num = 0;
            CHECK(this.ftdi.Write(tx, tx.Length, ref num));
            if (num != tx.Length)
                throw new System.Exception("FTDI transmit failure: " + num.ToString() + "/" + tx.Length.ToString());

            // Read
            byte[] rx = new byte[num];
            CHECK(this.ftdi.Read(rx, num, ref num));
            if (num != tx.Length)
                throw new System.Exception("FTDI receive failure: " + num.ToString() + "/" + tx.Length.ToString());

            // Merge bits from receive with transmit so that clock can be recovered
            for (int i = 0; i < num; i++)
                rx[i] = (byte)((tx[i] & SPI_OUT_PINS) | (rx[i] & SPI_IN_PINS));
            this.receive = new List<byte>(rx);
        }

        private string ToString(IEnumerable<byte> bs)
        {
            var sb = new StringBuilder();
            if (bs == null)
                return "<null>";
            foreach (byte b in bs)
            {
                sb.Append((b & SPI_CLK) != 0 ? 'C' : '_');
                sb.Append((b & SPI_nCS) != 0 ? '_' : 'S');
                sb.Append((b & SPI_MISO) != 0 ? 'R' : '_');
                sb.Append((b & SPI_MOSI) != 0 ? 'T' : '_');
                sb.Append('\n');
            }
            sb.Append('\n');
            return sb.ToString();
        }
        /// <summary>
        /// Human-readable value idiom
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "TRANSMIT\n"
                  + "========\n" + this.ToString(this.transmit)
                  + "RECEIVE\n"
                  + "=======\n" + this.ToString(this.receive);
        }
    }
}
