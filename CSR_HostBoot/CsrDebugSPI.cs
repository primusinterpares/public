﻿/*
 * Copyright (c) 2014 Primus Inter Pares ltd
 * All rights reserved.
 * 
 * http://primusinterpares.co.uk
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or other
 * materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may
 * be used to endorse or promote products derived from this software without specific
 * prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

using System;
using System.Collections.Generic;
using System.Text;

namespace CSR_SPI
{
    class CsrDebugSPI : IDisposable
    {
        private PRIMUS.SPI spi;

        public CsrDebugSPI(uint clock = 100000, uint index = 0)
        {
            this.spi = new PRIMUS.SPI(clock, index);
        }

        public void Dispose()
        {
            this.spi.Dispose();
        }

        /// <summary>
        /// Returns TRUE when the processor is responding
        /// </summary>
        public bool IsOnline
        {
            get
            {
                return this.Read(0, 0) != null;
            }
        }

        /// <summary>
        /// Read a single word
        /// </summary>
        /// <param name="address">address of word</param>
        /// <returns>value at word</returns>
        public UInt16 Read(UInt16 address)
        {
            var data = this.Read(address, 1);
            // Return the first (and only) word
            if (data != null)
                foreach (var d in this.Read(address, 1))
                    return d;
            // An error occurred
            throw new System.Exception("SPI Read 0x" + address.ToString("X4") + " failed");
        }

        /// <summary>
        /// Read a block of words
        /// </summary>
        /// <param name="address">address of start of block</param>
        /// <param name="count">number of words in block</param>
        /// <returns>innumerable words or null on error</returns>
        public IEnumerable<UInt16> Read(UInt16 address, UInt16 count)
        {
            // Initialisation
            spi.CS = false;
            spi.Clock(false);

            // "Clocking in"
            spi.Clock();
            spi.Clock();
            spi.CS = true;

            // CSR100x read command
            spi.TX(0x03);

            // Address
            spi.TX((byte)(address >> 8));
            spi.TX((byte)(address & 0xFF));

            // Echo
            spi.TX(0xFF);
            spi.TX(0xFF);

            // Data
            for (uint i = 0; i < count; i++)
            {
                spi.TX(0xFF);
                spi.TX(0xFF);
            }

            // "Clocking out"
            spi.CS = false;
            spi.Clock();
            spi.Clock();

            // Do it
            spi.Go();

            // Ignore command, address
            spi.RX();
            spi.RX();
            spi.RX();

            // Check echo
            if (spi.RX() == 0x03
             && spi.RX() == (byte)(address >> 8))
            {
                // Read data
                var d = new List<UInt16>();

                // Read data words
                for (uint i = 0; i < count; i++)
                {
                    UInt16 data;
                    data = (UInt16)(spi.RX() << 8);
                    data = (UInt16)(data | spi.RX());
                    d.Add(data);
                }
                return d;
            }

            return null;
        }

        /// <summary>
        /// Write a single word
        /// </summary>
        /// <param name="address">address of word</param>
        /// <param name="word">value of word</param>
        public void Write(UInt16 address, UInt16 word)
        {
            this.Write(address, new UInt16[1] { word });
        }

        /// <summary>
        /// Write a block of words
        /// </summary>
        /// <param name="address">address of start of block</param>
        /// <param name="data">value of block</param>
        public void Write(UInt16 address, IEnumerable<UInt16> data)
        {
            // Initialisation
            spi.CS = false;
            spi.Clock(false);

            // "Clocking in"
            spi.Clock();
            spi.Clock();
            spi.CS = true;

            // CSR100x write command
            spi.TX(0x02);

            // Address
            spi.TX((byte)(address >> 8));
            spi.TX((byte)(address & 0xFF));

            // Data
            foreach (UInt16 d in data)
            {
                spi.TX((byte)(d >> 8));
                spi.TX((byte)(d & 0xFF));
            }

            // "Clocking out"
            spi.CS = false;
            spi.Clock();
            spi.Clock();

            // Do it
            spi.Go();
        }

        /// <summary>
        /// Write word with verify
        /// </summary>
        /// <returns>TRUE on success</returns>
        public bool WriteVerify(UInt16 address, UInt16 word)
        {
            this.Write(address, word);
            try
            {
                return this.Read(address) == word;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Write block with verify
        /// </summary>
        /// <returns>TRUE on success</returns>
        public bool WriteVerify(UInt16 address, IEnumerable<UInt16> words)
        {
            this.Write(address, words);
            var verify = this.Read(address, (UInt16)System.Linq.Enumerable.Count(words));
            return verify != null && System.Linq.Enumerable.SequenceEqual(words, verify);
        }

        /// <summary>
        /// Reads a memory image from the XUV file
        /// </summary>
        /// <param name="tr">Stream of XUV data</param>
        /// <returns>mapping of address:word</returns>
        public static IDictionary<UInt16, UInt16> ReadImage(System.IO.TextReader tr)
        {
            var d = new Dictionary<UInt16, UInt16>();

            for (uint i=1; ; i++)
            {
                // Read every line...
                string line = tr.ReadLine();

                // End of file?
                if (line == null)
                    return d;

                // Line starts with '@00'
                if (!line.StartsWith("@00"))
                    throw new System.Exception("Invalid XUV line:"+i.ToString()+" '"+line+"'");

                // Parse address, dataDecode address
                var parse = line.Substring(3).Split(' ');
                UInt16 address, data;
                try
                {
                    address = UInt16.Parse(parse[0], System.Globalization.NumberStyles.HexNumber);
                    data = UInt16.Parse(parse[3], System.Globalization.NumberStyles.HexNumber);
                }
                catch
                {
                    throw new System.Exception("Syntax error line:" + i.ToString() + " '" + line + "'");
                }

                // Add to dictionary
                if (d.ContainsKey(address))
                    throw new System.Exception("Duplicate address line:" + i.ToString() + " '" + line + "'");
                d.Add(address, data);
            }
        }
    }
}
